#Makefile for person application

BIN=bin
SRC=src
CC=gcc
DOC=doc

CFLAGS=-Wall -g -pedantic --std=c99 -D_DEFAULT_SOURCE

OBJS=$(patsubst $(SRC)/%.c,$(BIN)/%.o,$(wildcard $(SRC)/*.c))

$(info src:$(wildcard $(SRC)/*.c))

$(info *objs:$(OBJS))

$(BIN)/person: $(BIN) $(OBJS)
		$(CC) -o $(BIN)/person $(OBJS) -l cunit ${CFLAGS}
		
$(BIN)/person.o:$(SRC)/person.c
		$(CC) -c $< -o $@ $(CFLAGS)

$(BIN)/main.o:$(SRC)/main.c
		$(CC) -c $< -o $@ $(CFLAGS)
		
$(BIN):
		mkdir $(BIN)	
		
clean:
		$(RM) -r $(BIN)
				
all: clean $(BIN)/person

.PHONY: clean all
